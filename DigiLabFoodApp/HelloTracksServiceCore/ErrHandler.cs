using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.IO;
using System.Globalization;
using System.Diagnostics;

namespace DigiFoodLabServiceCore
{
    public class ErrHandler
    {
        public ErrHandler()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static void WriteError(string strMessage, string strRequestID)
        {
            try
            {
                string path = "~/Error/" + DateTime.Today.ToString("dd-MM-yy") + ".txt";
                if (!File.Exists(HttpContext.Current.Server.MapPath(path)))
                {
                    File.Create(HttpContext.Current.Server.MapPath(path)).Close();
                }
                using (StreamWriter w = File.AppendText(HttpContext.Current.Server.MapPath(path)))
                {
                    w.WriteLine("\r\nLog Entry:");
                    w.WriteLine("{0}", DateTime.Now.ToString(CultureInfo.InvariantCulture));
                    string err = "Error in: " + HttpContext.Current.Request.Url.ToString() + ". Error Message:" + strMessage;
                    w.WriteLine(err);
                    w.WriteLine("_______________________________________");
                    w.Flush();
                    w.Close();
                }
            }
            catch (Exception ex)
            {
                EventLog eventLog1 = new EventLog();
                eventLog1.Source = "HelloTracksWebAPI";
                eventLog1.WriteEntry("Error in: " + HttpContext.Current.Request.Url.ToString());
                eventLog1.WriteEntry(ex.Message, EventLogEntryType.Information);
            }
        }
    }
}
