﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace DigiFoodLabServiceCore
{
    class EmailHelper
    {
        public static string SendEmail(string sender, string recipient, string EmSuj, string EmMsg, string RequestId)
        {
            string sent = "Succeed";
            SmtpClient sendmail = new SmtpClient();
            try
            {
                sendmail.Host = "10.10.14.41";
                sendmail.Port = 25;
                sendmail.Send(sender, recipient, EmSuj, EmMsg);
                ErrHandler.WriteError("Email sent to "+ recipient, RequestId);
                return sent;
                
            }
            catch (SmtpFailedRecipientException ex)
            {
                ErrHandler.WriteError("Exception occurred in SignIn: Couldn't sent email to " + recipient + " "+ ex.Message + " " + ex.StackTrace, RequestId);
                return ex.Message;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
    }
}
