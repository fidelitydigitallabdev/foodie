﻿using System;
using System.Collections.Generic;
using System.Data;

using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DigiLabFoodApp.Models;
using DigiLabFoodApp.ViewModel;

namespace DigiLabFoodApp.Controllers
{
    [Authorize]
    public class OrdersController : Controller
    {
        public OrdersController()
        {
           
        }
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Orders
        public ActionResult Index()
        {
            return View(db.Orders.ToList());
        }

        //GET : Orders/GetOrders/{id}
        [Route("me/orders", Name ="MyOrders")]
        public ActionResult GetOrders()
        {
            ApplicationUser applicationUser = db.Users.FirstOrDefault(o => o.Email == HttpContext.User.Identity.Name);
            //var menu = new Menu
            //{

            //    CreatedAt = DateTime.Now,
            //    MenuDay = DayOfWeek.Monday,
            //    MenuDetail = "Yummmy",
            //    MenuName = "Yam and Egg",
            //    MenuOption = 1,
            //    MenuPrice = 59.40m,
            //    MenuStatus = MenuStatus.Active,
            //    MenuType = MenuType.Dinner,
            //    UpdatedAt = DateTime.Now,
            //    Vendor = applicationUser
            //};
            //var orderQuantity = 3;
            //var order = new Order
            //{
            //    Menu = menu,
            //    CreatedAt = DateTime.Now,
            //    OrderDate = DateTime.Now,
            //    OrderReference = "DLX",
            //    OrderStatus = OrderStatus.Pending,
            //    UpdatedAt = DateTime.Now,
            //    User_Id = applicationUser.Id,
            //    OrderQuantity = orderQuantity,
            //    OrderPrice = orderQuantity * menu.MenuPrice
            //};

            //db.Orders.Add(order);
            //db.SaveChanges();


            List<OrderViewModel> result = new List<OrderViewModel>();

            try
            {
                result = db.Database.SqlQuery<OrderViewModel>("exec GetUserOrderItems @UserId", new SqlParameter("@UserId", applicationUser.Id)).ToList();
                
            }
            catch (Exception ex)
            {
                //log 
                
            }

            return View(result);

            
        }
        // GET: Orders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // GET: Orders/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OrderId,UserId,OrderReference,OrderDate,OrderStatus,CreatedAt,UpdatedAt")] Order order)
        {
            if (ModelState.IsValid)
            {
                db.Orders.Add(order);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(order);
        }

        // GET: Orders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OrderId,UserId,OrderReference,OrderDate,OrderStatus,CreatedAt,UpdatedAt")] Order order)
        {
            if (ModelState.IsValid)
            {
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(order);
        }

        // GET: Orders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Order order = db.Orders.Find(id);
            db.Orders.Remove(order);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
