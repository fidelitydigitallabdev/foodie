﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DigiLabFoodApp.Models;
using DigiLabFoodApp.ViewModel;

namespace DigiLabFoodApp.Controllers
{
    [Authorize]
    public class PaymentsController : Controller
    {
        private string  UserId { get; set; }
        private ApplicationDbContext db = new ApplicationDbContext();
        
        

        // GET: Payments
        public ActionResult Index()
        {
            return View(GetPaymentDetails());
        }

        
        public ActionResult Invoice(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return View(GetOrderItems(id, UserId));
        }
        //[Route("D/{id}")]
        // GET: Payments/Details/5
        public ActionResult Details(int? id)
        {
            ApplicationUser user = db.Users.FirstOrDefault( o=> o.Email == HttpContext.User.Identity.Name);
            //Payment paymentw = new Payment
            //{
            //    ApplicationUser = user,
            //    ApplicationUser_Id = user.Id,
            //    PaymentDate = DateTime.Now,
            //    OrderReference = "DLD",
            //    PaymentReference = "NIWNDWFUWF",
            //    PaymentStatus = PaymentStatus.Paid,
            //    TransactionAmount = 2900M
            //};

            //db.Payments.Add(paymentw);
            //db.SaveChanges();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Payment payment = db.Payments.FirstOrDefault(o => o.PaymentId == id && o.ApplicationUser_Id == user.Id);

            if (payment == null)
            {
                return HttpNotFound();
            }


            return View(GetOrderItems(id, user.Id));
           
        }

        // GET: Payments/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Payments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PaymentId,OrderReference,PaymentReference,TransactionAmount,PaymentStatus,CardId")] Payment payment)
        {
            if (ModelState.IsValid)
            {
                db.Payments.Add(payment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(payment);
        }

        // GET: Payments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Payment payment = db.Payments.Find(id);
            if (payment == null)
            {
                return HttpNotFound();
            }
            return View(payment);
        }

        // POST: Payments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PaymentId,OrderReference,PaymentReference,TransactionAmount,PaymentStatus,CardId")] Payment payment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(payment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(payment);
        }

        // GET: Payments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Payment payment = db.Payments.Find(id);
            if (payment == null)
            {
                return HttpNotFound();
            }
            return View(payment);
        }

        // POST: Payments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Payment payment = db.Payments.Find(id);
            db.Payments.Remove(payment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        public List<PaymentHistoryViewModel> GetPaymentDetails()
        {
            List<PaymentHistoryViewModel> result = new List<PaymentHistoryViewModel>();

            ApplicationUser user = db.Users.FirstOrDefault(o => o.Email == HttpContext.User.Identity.Name);
            try
            {
                result = db.Database.SqlQuery<PaymentHistoryViewModel>("exec GetPaymentsForUser @UserId", new SqlParameter("@UserId", user.Id)).ToList();

            }
            catch (Exception ex)
            {


            }
            return result;
        }

        public OrderDetailsPage GetOrderItems(int? id, string userId)
        {
            List<OrderDetailViewModel> result = new List<OrderDetailViewModel>();
            OrderDetailsPage orderDetails = new OrderDetailsPage();

            try
            {
                result = db.Database.SqlQuery<OrderDetailViewModel>("exec GetSingleOrderDetailForUser @UserId, @PaymentId", 
                    new SqlParameter("@UserId", userId),
                    new SqlParameter("@PaymentId", id)
                    ).ToList();
                
                //get total charge after multiplying the unit price and quantity
                Decimal totalCharge = 0.00M;
                foreach (var item in result)
                {
                    totalCharge += item.TotalPrice;
                }
                Decimal chargePerUnit = 0.00M;

                //get total charge before multiplying the unit price and quantity
                foreach (var item in result)
                {
                    chargePerUnit += item.MenuPrice;
                }



                // add a new order of service charge to the list
                OrderDetailViewModel serviceChargeModel = new OrderDetailViewModel
                {
                    ItemNo = result.Count + 1,
                    MenuName = "Service Charge",
                    OrderQuantity = 1,
                    MenuDetail = "Service charge on Order",
                    MenuPrice = Convert.ToDecimal(ConfigurationManager.AppSettings["ServiceCharge"].ToString().Trim()) / 100 * chargePerUnit,
                   
                    TotalPrice = Convert.ToDecimal(ConfigurationManager.AppSettings["ServiceCharge"].ToString().Trim())  / 100 * totalCharge
                };

                result.Add(serviceChargeModel);


                //get the total charge after adding the service charge
                var orderTotalCharge = 0.0m;
                foreach (var item in result)
                {
                    orderTotalCharge += item.TotalPrice;
                }

                //charge 5% charge on the order
                decimal vatOnOrder = orderTotalCharge * Convert.ToDecimal(ConfigurationManager.AppSettings["VAT"].ToString().Trim()) / 100;

                //then this will be the final charge payable for this order 
                Decimal TotalOrderPrice = orderTotalCharge + vatOnOrder;

                orderDetails.OrderDetail = result;
                orderDetails.TotalCharge = TotalOrderPrice;
                orderDetails.VatOnOrder = vatOnOrder;
                orderDetails.OrderTotalCharge = orderTotalCharge;

                var users = db.Users.FirstOrDefault(o => o.Id == userId);

                var userAddress = from user in db.Users
                                   join address in db.Addresses on user.Address.AddressId equals address.AddressId
                                   where user.Id == userId
                                   select new { City = address.City, Street = address.Street, State = address.State, Country = address.Country, HouseNumber = address.HouseNumber  };
                var customerAddress = new Address();

                //fix later
                foreach (var item in userAddress)
                {
                    customerAddress.City = item.City;
                    customerAddress.Country = item.Country;
                    customerAddress.HouseNumber = item.HouseNumber;
                    customerAddress.State = item.State;
                    customerAddress.Street = item.Street;
                }

                orderDetails.BuyerAddress = customerAddress;
            }
            catch (Exception ex)
            {
                //log exception
            }
            return orderDetails;
        }
        //private List<Payment> GetAllPaymentDetail()
        //{

        //}
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
