namespace DigiLabFoodApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedReviewsColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reviews", "Subject", c => c.String(maxLength: 30));
            AddColumn("dbo.Reviews", "Comment", c => c.String(maxLength: 100));
            AddColumn("dbo.Reviews", "StarRank", c => c.String(maxLength: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Reviews", "StarRank");
            DropColumn("dbo.Reviews", "Comment");
            DropColumn("dbo.Reviews", "Subject");
        }
    }
}
