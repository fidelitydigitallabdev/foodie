namespace DigiLabFoodApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedlocationandbranch : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        AddressId = c.Int(nullable: false, identity: true),
                        HouseNumber = c.String(maxLength: 10),
                        Street = c.String(maxLength: 50),
                        City = c.String(maxLength: 50),
                        State = c.String(maxLength: 50),
                        Country = c.String(maxLength: 50),
                        BranchToServeIfVendor_BranchId = c.Int(),
                    })
                .PrimaryKey(t => t.AddressId)
                .ForeignKey("dbo.Branches", t => t.BranchToServeIfVendor_BranchId)
                .Index(t => t.BranchToServeIfVendor_BranchId);
            
            CreateTable(
                "dbo.Branches",
                c => new
                    {
                        BranchId = c.Int(nullable: false, identity: true),
                        BranchCode = c.String(maxLength: 50),
                        BranchName = c.String(maxLength: 50),
                        DirectoryName = c.String(maxLength: 50),
                        BranchAddress = c.String(maxLength: 100),
                        PhysicalLocation = c.String(maxLength: 100),
                        CityCode = c.String(maxLength: 10),
                        StateCode = c.String(maxLength: 10),
                    })
                .PrimaryKey(t => t.BranchId);
            
            AddColumn("dbo.AspNetUsers", "Address_AddressId", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "Address_AddressId");
            AddForeignKey("dbo.AspNetUsers", "Address_AddressId", "dbo.Addresses", "AddressId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "Address_AddressId", "dbo.Addresses");
            DropForeignKey("dbo.Addresses", "BranchToServeIfVendor_BranchId", "dbo.Branches");
            DropIndex("dbo.Addresses", new[] { "BranchToServeIfVendor_BranchId" });
            DropIndex("dbo.AspNetUsers", new[] { "Address_AddressId" });
            DropColumn("dbo.AspNetUsers", "Address_AddressId");
            DropTable("dbo.Branches");
            DropTable("dbo.Addresses");
        }
    }
}
