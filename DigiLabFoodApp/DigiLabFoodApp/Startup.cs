﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DigiLabFoodApp.Startup))]
namespace DigiLabFoodApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
