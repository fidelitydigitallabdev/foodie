﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DigiLabFoodApp.Models
{
    public class Address
    {
      
        public int AddressId { get; set; }
        [MaxLength(10)]
        public string HouseNumber { get; set; }

        [MaxLength(50)]
        public string Street { get; set; }
        [MaxLength(50)]
        public string City { get; set; }
        [MaxLength(50)]
        public string State { get; set; }

        [MaxLength(50)]
        public string Country { get; set; }

        
        public Branch BranchToServeIfVendor { get; set; }
    }

    public class Branch
    {
        public int BranchId { get; set; }

        [MaxLength(50)]
        public string BranchCode { get; set; }

        [MaxLength(50)]
        public string BranchName { get; set; }

        [MaxLength(50)]
        public string DirectoryName { get; set; }

        [MaxLength(100)]
        public string BranchAddress { get; set; }

        [MaxLength(100)]
        public string PhysicalLocation { get; set; }

        [MaxLength(10)]
        public string CityCode { get; set; }

        [MaxLength(10)]
        public string StateCode { get; set; }

    }
}