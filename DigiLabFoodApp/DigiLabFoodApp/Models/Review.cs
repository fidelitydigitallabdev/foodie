﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DigiLabFoodApp.Models
{
    public class Review
    {
        public int ReviewId { get; set; }
        [MaxLength(30)]
        public string Subject { get; set; }

        [MaxLength(100)]
        public string Comment { get; set; }

        [MaxLength(2)]
        public string StarRank { get; set; }
        
    }
}