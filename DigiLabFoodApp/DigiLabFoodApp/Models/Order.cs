﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DigiLabFoodApp.Models
{
    public class Order
    {
        public int OrderId { get; set; }

        public string User_Id { get; set; }
        
        [Required]
        public string OrderReference { get; set; }

        [Required]
        public Menu Menu { get; set; }

        [Required]
        public DateTime OrderDate { get; set; }

        [Required]
        public OrderStatus OrderStatus { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        [Required]
        public DateTime UpdatedAt { get; set; }

        public int OrderQuantity { get; set; }

        [Required]
        public Decimal OrderPrice {
            get; set;
        }

    }

    public enum OrderStatus
    {
        OnHold, Pending, Open, Closed
    }
}