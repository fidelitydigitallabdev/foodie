﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DigiLabFoodApp.Models
{
    public class Menu
    {

        public int MenuId { get; set; }
        [Required]
        public ApplicationUser Vendor { get; set; }
        //[Required]
        public string MenuName { get; set; }

        //[Required]
        public DayOfWeek MenuDay { get; set;}

        //[Required]
        public string MenuDetail { get; set; }

        //[Required]
        public MenuType MenuType { get; set; }

        //[Required]
        public int MenuOption { get; set; }

        //[Required]
        public MenuStatus MenuStatus { get; set; }

        //[Required]
        public Decimal MenuPrice { get; set; }

        
        public DateTime CreatedAt { get; set; }

        //[Required]
        public DateTime UpdatedAt { get; set; }
    }

    public enum MenuType
    {
        BreakFast = 1, Lunch, Dinner
    }

    public enum MenuStatus
    {
        Inactive, Active
    }


}