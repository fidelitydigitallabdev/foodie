﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DigiLabFoodApp.Models
{
    public class Appointment
    {
        public int AppointmentId { get; set; }
        public DateTime AppointmentDate { get; set; }
        public string VisitorName { get; set; }
        public string PhoneNumber { get; set; }
        public string Location { get; set; }
    }
}