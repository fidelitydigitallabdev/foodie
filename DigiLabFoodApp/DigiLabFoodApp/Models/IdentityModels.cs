﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DigiLabFoodApp.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        
        public List<Order> Orders { get; set; }
        public List<Payment> Payments { get; set; }
        public UserType UserClass { get; set; }
        public List<Review> Reviews { get; set; }
        public Address Address { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public enum UserType
        {
            Vendor = 1, Subscriber 
        }
    }

   
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<DigiLabFoodApp.Models.Menu> Menus { get; set; }
        public System.Data.Entity.DbSet<DigiLabFoodApp.Models.Order> Orders { get; set; }
        public System.Data.Entity.DbSet<DigiLabFoodApp.Models.Payment> Payments { get; set; }
        public System.Data.Entity.DbSet<DigiLabFoodApp.Models.Address> Addresses { get; set; }
        public System.Data.Entity.DbSet<DigiLabFoodApp.Models.Branch> Branches { get; set; }

        public System.Data.Entity.DbSet<DigiLabFoodApp.Models.Review> Reviews { get; set; }
    }
}