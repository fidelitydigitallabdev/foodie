﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DigiLabFoodApp.Models
{
    public class Payment
    {
        
        [Required]
        public int PaymentId { get; set; }

        [Required]
        public string OrderReference { get; set; }

        [Required]
        public string PaymentReference { get; set; }

        [Required]
        public Decimal TransactionAmount { get; set; }

        [Required]
        public PaymentStatus PaymentStatus { get; set; }

        public DateTime PaymentDate { get; set; }
        public int CardId { get; set; }

        [ForeignKey("ApplicationUser")]

        public string ApplicationUser_Id { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

    }

    public enum PaymentStatus
    {

        Pending, Paid
    }
}