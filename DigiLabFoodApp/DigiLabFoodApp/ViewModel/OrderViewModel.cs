﻿using DigiLabFoodApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace DigiLabFoodApp.ViewModel
{
    public class OrderViewModel
    {
        public int OrderViewModelId { get; set; }
        public string OrderReference { get; set; }
        public Decimal TotalPrice { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public DateTime OrderDate { get; set; }
    }
}