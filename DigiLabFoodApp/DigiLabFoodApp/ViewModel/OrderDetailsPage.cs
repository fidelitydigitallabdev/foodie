﻿using DigiLabFoodApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DigiLabFoodApp.ViewModel
{
    public class OrderDetailsPage : OrderDetails
    {
        public int OrderDetailsPageId {get; set;}
        public Address BuyerAddress { get; set; }
        public Address SellerAddress { get; set; }
    }

    public class OrderDetails
    {
        public List<OrderDetailViewModel> OrderDetail { get; set; }
        public decimal VatOnOrder { get; set; }
        public decimal TotalCharge { get; set; }
        public decimal OrderTotalCharge { get; set; }
    }
}