﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DigiLabFoodApp.ViewModel
{
    public class OrderDetailViewModel
    {
        public int OrderDetailViewModelId {get; set;}
        public long ItemNo { get; set; }
        public string MenuName { get; set; }
        public string MenuDetail { get; set; }
        public int OrderQuantity { get; set; }
        public Decimal MenuPrice { get; set; }
        public Decimal TotalPrice { get; set; }
        
    }
}