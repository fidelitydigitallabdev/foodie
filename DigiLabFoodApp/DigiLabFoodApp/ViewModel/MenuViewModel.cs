﻿using DigiLabFoodApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DigiLabFoodApp.ViewModel
{
    public class MenuViewModel
    {
        public int MenuViewModelId { get; set; }
        [Required]
        public ApplicationUser  Vendor { get; set; }
        [Required]
        public string MenuName { get; set; }

        [Required]
        public DayOfWeek MenuDay { get; set; }

        [Required]
        public string MenuDetail { get; set; }

        [Required]
        public int MenuType { get; set; }

        [Required]
        public int MenuOption { get; set; }

        [Required]
        public MenuStatus MenuStatus { get; set; }

        [Required]
        public Decimal MenuPrice { get; set; }

    }
}