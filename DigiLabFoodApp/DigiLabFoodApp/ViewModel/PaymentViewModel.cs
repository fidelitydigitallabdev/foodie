﻿using DigiLabFoodApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DigiLabFoodApp.ViewModel
{
    public class PaymentHistoryViewModel
    {
        public int PaymentHistoryViewModelId { get; set; }
        [Required]
        public int PaymentId { get; set; }

        public string PaymentReference { get; set; }

        [Required]
        public Decimal TransactionAmount { get; set; }

        [Required]
        public PaymentStatus PaymentStatus { get; set; }

        [Required]
        public string OrderReference { get; set; }

        public DateTime PaymentDate { get; set; }
    }
}